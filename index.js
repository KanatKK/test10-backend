const express = require("express");
const cors = require("cors");
const mysql = require("mysql");
const db = require("./mysql");
const items = require("./app/items");
const config = require("./config");
const app = express();
const port = 8000;

const connection = mysql.createConnection(config.db);

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

connection.connect((err) => {
  if (err) {
    console.log(err);
    throw err;
  }

  app.use("/", items(db(connection)));

  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
  });
});