module.exports = (db) => ({
    getItems(resource) {
        return new Promise((res, rej) => {
            db.query("SELECT * FROM ??", [resource], (err, result) => {
                if (err) {
                    rej(err);
                }
                const shortInfo = [];
                result.map( r => {
                    if (r.image !== undefined) {
                        shortInfo.push({
                            title: r.title,
                            id: r.id,
                            image: r.image,
                            date: r.postTime
                        });
                    } else {
                        shortInfo.push({title: r.title, id: r.id, date: r.postTime});
                    }
                });
                res(shortInfo);
            });
        });
    },
    getItem(resource, id) {
        return new Promise((res, rej) => {
            db.query("SELECT * FROM ?? WHERE id = ?", [resource, id], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },
    getComments(resource, id) {
        return new Promise((res, rej) => {
            db.query("SELECT * FROM ?? WHERE posts_id = ?", [resource, id], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },
    createItem(resource, data) {
        return new Promise((res, rej) => {
            db.query("INSERT INTO ?? SET ?", [resource, data], (err) => {
                if (err) {
                    rej(err);
                }
                res(data);
            });
        });
    },
    deleteItem(resource, id) {
        return new Promise((res, rej) => {
            db.query("DELETE FROM "+ resource +" WHERE id = "+ id +"", [resource, id], (err) => {
                if (err) {
                    console.log(err);
                    rej(err);
                }
            });
        });
    },
});