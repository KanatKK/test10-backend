const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const createRouter = (db) => {
    router.get("/posts", async (req, res) => {
        try {
            const posts = await db.getItems("posts");
            res.send(posts);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/posts/:id", async (req, res) => {
        const response = await db.getItem("posts" ,req.params.id);
        res.send(response[0]);
    });

    router.post("/posts", upload.single("image"), async (req, res) => {
        const posts = req.body;
        if (req.file) {
            posts.image = req.file.filename;
        }
        const newPosts = await db.createItem("posts", posts);
        res.send(newPosts);
    });

    router.delete("/posts/:id", async (req) => {
        try {
            await db.deleteItem("posts", req.params.id);
        } catch (e) {
            console.log(e);
        }
    });

    router.get("/comments", async (req, res) => {
        try {
            const comments = await db.getItems("comments");
            res.send(comments);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/comments/:id", async (req, res) => {
        const response = await db.getComments("comments" ,req.params.id);
        res.send(response);
    });

    router.post("/comments", upload.single("image"), async (req, res) => {
        const comments = req.body;
        const newComments = await db.createItem("comments", comments);
        res.send(newComments);
    });

    router.delete("/comments/:id", async (req) => {
        try {
            await db.deleteItem("comments", req.params.id);
        } catch (e) {
            console.log(e);
        }
    });

    return router;
};

module.exports = createRouter;